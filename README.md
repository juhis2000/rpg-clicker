I made this project to practice file saving / editing / management which is a
very essential in an RPG. I decided to use .JSON files for the game because of 
the easy parsing tools built-in the Godot. The game was made using Godot 3 and
GDScript for mobile platforms. All the sprites are made by me as well.    
The game also includes an item system which is used for the inventory and 
shop where you can buy/sell items. Each item can be edited to have any kind
of boost for the player e.g. more mana / hp. After leveling up the player can
update the stats of the character.    
The game world is based on a tower that descends infinitely. Floors x1-x9 have
the same 3 enemies and on the floor x0 there is a boss battle.    
Note that this game is not complete because of the time it would take to make.    
Gameplay    
Tap the screen until the enemy dies and use items which may use mana as well to
keep your hp above 0.    
Video of the gameplay:    
![](video.mp4)  
See also: https://godotengine.org/license