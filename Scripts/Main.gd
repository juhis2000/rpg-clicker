extends Node

onready var slash_node = preload("res://Scenes/Slash.tscn");

var needed_xp;

var enemy;
var enemy_hp;
var enemy_max_dmg;
var enemy_min_dmg;
var enemy_max_xp;
var enemy_min_xp;
var enemy_max_crystals;
var enemy_min_crystals;

func _ready():
	Globals.new_items = [];
	Globals.adventure += 1;
	
	needed_xp = 10 * Globals.lvl + 90;
	_update_all_stats();
	
	for i in $Items.get_child_count()-1:
		if(String(Globals.equipped_items[i]) != ""):
			get_node("Items/Item" + String(i+1)).connect("pressed", self, "_item_pressed", [i]);
			var image = "res://Sprites/Inventory/Items/" + String(Globals.equipped_items[i]) + ".png"
			$Items.get_child(i).texture_normal = load(image);
	
	randomize();
	_spawn_enemy();
	$ClickableArea.connect("pressed", self, "_clicked");

func _process(delta):
	if(enemy_hp <= 0):
		_enemy_dead();
	
	$ClickableArea/EnemyHitProgressBar.value += $ClickableArea/EnemyHitProgressBar.max_value / $ClickableArea/EnemyHitProgressBar.max_value * delta;
	
	for i in 4:
		if(has_node("SkillTimer" + String(i))):
			var item = Globals.equipped_items[i];
			get_node("Items/Item" + String(i+1) + "/CdProgress"). value -= 100 / int(Globals.item_json_result[item]["cd"]) * delta;

func _enemy_dead():
	if(rand_range(0, 100) <= Globals.loot_luck):
			var item = Globals.enemy_json_result[Globals._floor]["loot"][Funcs.irand_range(0, Globals.enemy_json_result[Globals._floor]["loot"].size()-1)];
			Globals.items.append(item);
			Globals.new_items.append(item);
			
			# Make an item spawn list
			var control_node = Control.new();
			$ItemDrops/VBoxContainer.add_child(control_node);
			$ItemDrops/VBoxContainer.move_child(control_node, 0);
			var dropped_item = Sprite.new();
			control_node.add_child(dropped_item);
			dropped_item.texture = load("res://Sprites/Inventory/Items/" + item + ".png");
			dropped_item.scale = Vector2(0.5, 0.5);
			dropped_item.centered = false;
			if($ItemDrops/VBoxContainer.get_child_count() == 4):
				$ItemDrops/VBoxContainer.get_child(3).queue_free();
		
	Globals.crystals += Funcs.irand_range(int(enemy_min_crystals), int(enemy_max_crystals));
		
	Globals.xp += Funcs.irand_range(int(enemy_min_xp), int(enemy_max_xp));
	_update_all_stats();
	_spawn_enemy();
		
	needed_xp = 10 * Globals.lvl + 90;
	if(Globals.xp >= needed_xp):
		_level_up();

func _level_up():
	Globals.lp_gained += 1;
	Globals.xp -= needed_xp;
	Globals.lvl += 1;
	Globals.lp += 1;
	_update_all_stats();

func _item_pressed(i):
	if(!has_node("SkillTimer" + String(i))):
		var item = Globals.equipped_items[i];
		# Item actives
		Globals.hp += Globals.item_json_result[item]["a_hp"];
		if(Globals.hp > Globals.max_hp): Globals.hp = Globals.max_hp;
		Globals.mp += Globals.item_json_result[item]["a_mp"];
		if(Globals.mp > Globals.max_mp): Globals.mp = Globals.max_mp;
		enemy_hp -= Globals.item_json_result[item]["a_dmg"];
		
		# Show stats
		var _pos = get_node("Items/Item" + String(i+1)).rect_position;
		if(Globals.item_json_result[item]["a_hp"] > 0): Funcs._floating_text(Vector2(_pos.x+10, _pos.y-20), Color(0,1,0), String(Globals.item_json_result[item]["a_hp"]));
		if(Globals.item_json_result[item]["a_mp"] > 0): Funcs._floating_text(Vector2(_pos.x+10, _pos.y-20), Color(0,0,1), String(Globals.item_json_result[item]["a_mp"]));
		if(Globals.item_json_result[item]["a_dmg"] > 0): Funcs._floating_text(Vector2(_pos.x+10, _pos.y-20), Color(1,1,0), String(Globals.item_json_result[item]["a_dmg"]));
		
		_update_all_stats();
		
		if(Globals.item_json_result[Globals.equipped_items[i]]["uses"] == 1):
			Globals.equipped_items[i] = "";
			var _node = get_node("Items/Item" + String(i+1))
			_node.disconnect("pressed", self, "_item_pressed");
			_node.texture_normal = null;
			return;
		
		# CD timer
		get_node("Items/Item" + String(i+1) + "/CdProgress").value = 100;
		var timer = Timer.new();
		add_child(timer);
		timer.name = "SkillTimer" + String(i);
		timer.connect("timeout", self, "_on_skilltimer_timeout", [i]);
		timer.wait_time = Globals.item_json_result[item]["cd"];
		timer.start();

func _update_all_stats():
	Funcs._center("Main/HUD/Texts/XPText", String(Globals.xp) + "/" + String(needed_xp));
	Funcs._update_text("Main/HUD/Texts/HPText", Globals.hp);
	Funcs._update_bar("Main/HUD/Bars/HPBar", Globals.max_hp, Globals.hp);
	Funcs._center("Main/HUD/Texts/LVLText", String(Globals.lvl));
	$HUD/Texts/HPText.text = String(Globals.hp);
	$HUD/Texts/MPText.text = String(Globals.mp);
	$HUD/Texts/CRText.text = String(Globals.crystals);
	$HUD/Texts/FLText.text = String(Globals._floor).capitalize();
	if(enemy_hp != null): Funcs._center("Main/ClickableArea/EnemyHP", String(enemy_hp));

func _clicked():
	var slash = slash_node.instance();
	add_child(slash);
	slash.playing = true;
	slash.position = get_viewport().get_mouse_position();
	slash.rotation_degrees = Funcs.irand_range(0, 360);
	slash.connect("animation_finished", self, "_slash_finished", [slash]);
	
	enemy_hp -= Globals.dmg;
	Funcs._floating_text(get_viewport().get_mouse_position(), Color(1,1,0), String(Globals.dmg));
	_update_all_stats();

func _slash_finished(slash):
	slash.queue_free();

func _spawn_enemy():
	enemy = Globals.enemy_json_result[Globals._floor]["enemies"][Funcs.irand_range(0, Globals.enemy_json_result[Globals._floor]["enemies"].size()-1)];
	$ClickableArea/Sprite.texture = load("res://Sprites/Enemies/idle/" + enemy + ".png");
	var enemy_base_lvl = Globals.enemy_json_result[Globals._floor]["enemy_base_lvl"];
	enemy_hp = 9 * enemy_base_lvl + 5 - Funcs.irand_range(0, 3);
	enemy_max_dmg = 3 * enemy_base_lvl - 2;
	enemy_min_dmg = enemy_max_dmg - enemy_base_lvl;
	enemy_max_xp = enemy_base_lvl + 1;
	enemy_min_xp = enemy_max_xp - enemy_base_lvl;
	enemy_max_crystals = enemy_base_lvl;
	enemy_min_crystals = enemy_max_crystals - enemy_base_lvl;
	
	_update_all_stats();
	
	$ClickableArea/EnemyHitProgressBar.value = 0;
	$ClickableArea/EnemyHitProgressBar.max_value = Globals.enemy_json_result[Globals._floor]["hit_time"];
	$ClickableArea/EnemyHitTimer.wait_time = Globals.enemy_json_result[Globals._floor]["hit_time"];
	$ClickableArea/EnemyHitTimer.start();

func _on_EnemyHitTimer_timeout():
	$ClickableArea/EnemyHitProgressBar.value = 0;
	
	var _dmg = Funcs.irand_range(int(enemy_min_dmg), int(enemy_max_dmg));
	Globals.hp -= _dmg;
	Funcs._floating_text(Vector2(71, 103), Color(1,0,0), String(_dmg));
	_update_all_stats();
	if(Globals.hp <= 0):
		Globals.hp = Globals.max_hp/2;
		get_tree().change_scene("res://Scenes/Score.tscn");
	
func _on_skilltimer_timeout(i):
	get_node("SkillTimer" + String(i)).queue_free();

func _on_EscapeButton_pressed():
	Funcs._save();
	get_tree().change_scene("res://Scenes/Score.tscn");
