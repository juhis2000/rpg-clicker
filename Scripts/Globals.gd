extends Node

# Save!
var max_hp;
var hp;
var dmg;
var max_mp;
var mp;
var xp;
var items = [];
var crystals;
var lvl;
var lp;
var adventure;

var loot_luck;
var loot = "No loot";

var equipped_items;
#

# Dont't save!
var _floor = "floor1";
var floor_count;
var enemy_json_result;
var item_json_result;
var new_items;
var lp_gained = 0;
#

func _ready():
	# Copy the player writable files on the device memory for reading
	if(!Directory.new().file_exists("user://PlayerStats.json")):
		var dir = Directory.new();
		dir.copy("res://Stats/PlayerStats.json", "user://PlayerStats.json");
	# Load save data
	var save_file = File.new();
	save_file.open("user://PlayerStats.json", save_file.READ)
	var save_json = save_file.get_as_text();
	var save_json_result = JSON.parse(save_json).result;
	save_file.close();
	
	crystals = save_json_result["crystals"];
	xp = save_json_result["xp"];
	dmg = save_json_result["dmg"];
	hp = save_json_result["hp"];
	max_hp = save_json_result["max_hp"];
	mp = save_json_result["mp"];
	max_mp = save_json_result["max_mp"];
	lvl = save_json_result["lvl"];
	loot_luck = save_json_result["loot_luck"];
	items = save_json_result["items"];
	lp = save_json_result["lp"];
	adventure = save_json_result["adventure"];
	
	# Load enemy data
	var file = File.new();
	file.open("res://Stats/EnemyStats.json", file.READ);
	var json = file.get_as_text();
	enemy_json_result = JSON.parse(json).result;
	file.close();
	
	floor_count = enemy_json_result.keys().size();
	
	# Load item data
	var item_file = File.new();
	item_file.open("res://Stats/ItemStats.json", file.READ);
	var item_json = item_file.get_as_text();
	item_json_result = JSON.parse(item_json).result;
	file.close();
	
	equipped_items = save_json_result["equipped_items"];