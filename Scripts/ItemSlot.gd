extends Control

var item_number; # position in the items list
var image;
var descr = "";

func _ready():
	image = "res://Sprites/Inventory/Items/" + Globals.items[item_number] + ".png";
	$TextureButton/Sprite.texture = load(image);

func _button_up():
	modulate = Color(1,1,1,1);

func _button_down():
	var item_name = Globals.items[item_number];
	var item = Globals.item_json_result[item_name];
	for i in item.keys().size():
		if(String(item.values()[i]) != String(0)):
			descr += (String(item.keys()[i]).capitalize() + " : " + String(item.values()[i]))
			descr += "\n"
	Funcs._add_description("Inventory", descr);
	descr = "";
	var _node = "Inventory";
	if(get_tree().get_root().has_node("Sell")): _node = "Sell";
	get_tree().get_root().get_node(_node + "/Inspect/Sprite").texture = load(image);
	get_tree().get_root().get_node(_node).selected_item = item_number;
	modulate = Color(0,1,0,1);
	if(get_tree().get_root().has_node("Sell")):
		get_tree().get_root().get_node("Sell").item_name = item_name;
