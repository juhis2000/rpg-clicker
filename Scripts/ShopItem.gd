extends Control

var item_number;
var image;
var descr = "";
var real_item_number;

func _ready():
	image = "res://Sprites/Inventory/Items/" + Globals.item_json_result.keys()[item_number] + ".png";
	$Button/Sprite.texture = load(image);

func _on_Button_pressed():
	var item_name = Globals.item_json_result.keys()[item_number];
	var item = Globals.item_json_result[item_name];
	for i in item.keys().size():
		if(String(item.values()[i]) != String(0)):
			descr += (String(item.keys()[i]).capitalize() + " : " + String(item.values()[i]))
			descr += "\n"
	Funcs._add_description("Shop", descr);
	descr = "";
	get_tree().get_root().get_node("Shop/Inspect/Sprite").texture = load(image);
	get_tree().get_root().get_node("Shop").selected_item = get_index();
	get_tree().get_root().get_node("Shop").item_name = item_name;