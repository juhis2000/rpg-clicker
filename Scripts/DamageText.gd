extends RichTextLabel

var a = 1;
var color;

func _ready():
	pass

func _process(delta):
	rect_position.y -= 0.7;
	
	add_color_override("default_color", Color(color.r, color.g, color.b, a));
	if(a > 0):
		a -= 0.025;
	else:
		queue_free();