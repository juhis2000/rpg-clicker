extends Node

var selected_item;
var item_name;

func _ready():
	$CrystalText.text = String(Globals.crystals);

func _process(delta):
	if(Input.is_action_just_pressed("ui_accept")): Funcs._save();

func _on_SellButton_pressed():
	if(selected_item != null):
		var price = Globals.item_json_result[item_name]["sell_for"];
		if(price != "-"):
			Globals.crystals += price;
			$CrystalText.text = String(Globals.crystals);
		Globals.items.remove(selected_item);
		$ScrollContainer._update_items();

func _on_ReturnButton_pressed():
	Funcs._save();
	get_tree().change_scene("res://Scenes/MainMenu.tscn");

func _on_GoToBuyButton_pressed():
	Funcs._save();
	get_tree().change_scene("res://Scenes/Shop.tscn");

func _on_GoToSellButton_pressed():
	Funcs._save();
	get_tree().change_scene("res://Scenes/Sell.tscn");