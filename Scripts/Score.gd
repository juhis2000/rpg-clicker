extends Node

var texts = ["Adventure", "LVL", "Floor"];

func _ready():
	$Adventure.set_bbcode("[center]" + String(Globals.adventure) + "[/center]");
	$LVL.set_bbcode("[center]" + "+" + String(Globals.lp_gained) + "[/center]");
	$Floor.set_bbcode("[center]" + String(Globals._floor).replace("floor", "") + "[/center]");
	var result = "";
	for i in Globals.new_items.size():
		result += String(Globals.new_items[i]) + "\n"
	if(result == ""): result = "-";
	$Container/Items.set_bbcode("[center]" + result + "[/center]");

func _on_ReturnButton_pressed():
	get_tree().change_scene("res://Scenes/DungeonFloors.tscn");
