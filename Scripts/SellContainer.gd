extends ScrollContainer

func _ready():
	$ItemList.columns = 2;
	randomize();
	_update_items();

func _update_items():
	if($ItemList.get_child_count() > 0):
		for j in $ItemList.get_child_count():
			$ItemList.get_child(j).queue_free();
	
	$ItemList.rect_min_size.y = Globals.items.size() * 9;
	for i in Globals.items.size():
		var item = load("res://Scenes/ItemSlot.tscn").instance();
		item.item_number = i;
		$ItemList.add_child(item);