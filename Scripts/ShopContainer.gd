extends ScrollContainer

func _ready():
	$ItemList.columns = 4;
	randomize();
	_update_items();

func _update_items():
	if($ItemList.get_child_count() > 0):
		for j in $ItemList.get_child_count():
			$ItemList.get_child(j).queue_free();
	
	$ItemList.rect_min_size.y = 16 * 9;
	for i in 16:
		var item = load("res://Scenes/ShopItem.tscn").instance();
		item.item_number = Funcs.irand_range(0, Globals.item_json_result.size()-1);
		item.real_item_number = i;
		$ItemList.add_child(item);