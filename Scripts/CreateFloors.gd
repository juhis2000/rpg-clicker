extends VBoxContainer

onready var floor_button = preload("res://Scenes/FloorButton.tscn");

func _ready():
	for i in Globals.floor_count:
		var floor_btn = floor_button.instance();
		floor_btn.floor_name = i;
		add_child(floor_btn);
		if(i == Globals.floor_count - 1):
			move_child(get_child(0), get_child_count());

func _on_TextureButton_pressed():
	get_tree().change_scene("res://Scenes/MainMenu.tscn");