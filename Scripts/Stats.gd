extends Node

var names = ["hp", "mp", "dmg", "loot_luck"];

func _ready():
	for i in $Buttons.get_child_count():
		$Buttons.get_child(i).connect("pressed", self, "_button_pressed", [i]);
	
	_update_all();

func _button_pressed(i):
	if(Globals.lp > 0):
		Globals.lp -= 1;
		if(i == 0): Globals.max_hp += 1;
		if(i == 1): Globals.max_mp += 1;
		if(i == 2): Globals.dmg += 1;
		if(i == 3): Globals.loot_luck += 1;
		_update_all();

func _update_all():
	$LPText.text = String(Globals.lp);
	$Texts/HPText.text = String(Globals.max_hp);
	$Texts/MPText.text = String(Globals.max_mp);
	$Texts/DMGText.text = String(Globals.dmg);
	$Texts/LootLuckText.text = String(Globals.loot_luck) + "%";

func _on_Return_pressed():
	Funcs._save();
	get_tree().change_scene("res://Scenes/MainMenu.tscn");