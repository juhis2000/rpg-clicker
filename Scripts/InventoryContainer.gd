extends ScrollContainer

export (String)var scene;

func _ready():
	_update_items();

func _update_items():
	if($ItemList.get_child_count() > 0):
		for j in $ItemList.get_child_count():
			$ItemList.get_child(j).queue_free();
	
	$ItemList.rect_min_size.y = Globals.items.size() * 9;
	
	for i in Globals.items.size():
		var item = load("res://Scenes/ItemSlot.tscn").instance();
		item.item_number = i;
		$ItemList.add_child(item);
	
	if(scene == "Inventory"): for i in 4:
		if(Globals.equipped_items[i] != ""):
			var node = "Inventory/Buttons/InvSlot" + String(i+1);
			var image = "res://Sprites/Inventory/Items/" + Globals.equipped_items[i] + ".png";
			get_tree().get_root().get_node(node).texture_normal = load(image);