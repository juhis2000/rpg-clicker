extends Node

var buttons = ["Items", "Shop", "Stats", "Quests", "Dungeon", "Lore"];

func _ready():
	for i in $Buttons.get_child_count():
		var node = "Buttons/" + String(buttons[i]) + "Button";
		get_node(node).connect("pressed", self, "_on_button_pressed", [i]);

func _on_button_pressed(i):
	var go_to_room = buttons[i];
	if(buttons[i] == "Dungeon"): go_to_room = "DungeonFloors";
	if(buttons[i] == "Items"): go_to_room = "Inventory";
	get_tree().change_scene("res://Scenes/" + go_to_room + ".tscn");