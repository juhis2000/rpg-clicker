extends Node

var selected_item;
var selected_slot;
var slot_selected;

func _ready():
	for i in 4:
		get_node("Buttons/InvSlot" + String(i+1)).connect("pressed", self, "_on_invslot_pressed", [i]);

func _process(delta):
	if(Input.is_action_just_pressed("ui_accept")): Funcs._save();

func free_slot():
	for i in 4:
		if(Globals.equipped_items[i] == ""):
			var item_name = Globals.items[selected_item];
			Globals.equipped_items[i] = item_name;
			var node = "Inventory/Buttons/InvSlot" + String(i+1);
			var image = "res://Sprites/Inventory/Items/" + item_name + ".png";
			get_tree().get_root().get_node(node).texture_normal = load(image);
			Globals.items.remove(selected_item);
			
			# Item passive
			var _item = Globals.item_json_result[item_name]
			Globals.max_hp += _item["s_hp"];
			Globals.max_mp += _item["s_mp"];
			Globals.dmg += _item["s_dmg"];
			Globals.loot_luck += _item["s_loot_luck"];
			
			selected_item = null;
			return true;

func _on_EquipButton_pressed():
	if(selected_item != null):
		if(!slot_selected):
			if(free_slot()):
				$ScrollContainer._update_items();
				print("Item equipped!");
			else:
				print("No free slots!");
		else:
			print("Can't equip an equipped item!");
	else:
		print("No item selected!");

func _on_UnequipButton_pressed():
	if(slot_selected):
		if(Globals.equipped_items[selected_slot] != ""):
			# Take the stats back from the player
			var _item = Globals.item_json_result[Globals.equipped_items[selected_slot]]
			Globals.max_hp -= _item["s_hp"];
			Globals.max_mp -= _item["s_mp"];
			Globals.dmg -= _item["s_dmg"];
			Globals.loot_luck -= _item["s_loot_luck"];
			
			print("Item removed");
			Globals.equipped_items[selected_slot] = "";
			var node = "Inventory/Buttons/InvSlot" + String(selected_slot+1);
			get_tree().get_root().get_node(node).texture_normal = null;
			Globals.items.append(selected_item);
			slot_selected = false;
			selected_item = null;
			$ScrollContainer._update_items();
		else:
			print("Slot empty!");

func _on_invslot_pressed(i):
	if(Globals.equipped_items[i] != ""):
		selected_item = Globals.equipped_items[i];
		selected_slot = i;
		slot_selected = true;
		
		var descr = "";
		var item = Globals.item_json_result[selected_item];
		for j in item.keys().size():
			if(String(item.values()[j]) != String(0)):
				descr += (String(item.keys()[j]).capitalize() + " : " + String(item.values()[j]))
				descr += "\n"
		Funcs._add_description("Inventory", descr);
		var image = "res://Sprites/Inventory/Items/" + String(selected_item) + ".png";
		get_tree().get_root().get_node("Inventory/Inspect/Sprite").texture = load(image);

func _on_TextureButton_pressed():
	Funcs._save();
	get_tree().change_scene("res://Scenes/MainMenu.tscn");