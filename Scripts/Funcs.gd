extends Node

onready var floating_text_node = preload("res://Scenes/FloatingText.tscn");

func _save():
	var data = {
		"crystals": Globals.crystals,
		"hp": Globals.hp,
		"max_hp": Globals.max_hp,
		"xp": Globals.xp,
		"dmg": Globals.dmg,
		"mp": Globals.mp,
		"max_mp": Globals.max_mp,
		"lvl": Globals.lvl,
		"loot_luck": Globals.loot_luck,
		"items": Globals.items,
		"equipped_items": Globals.equipped_items,
		"lp": Globals.lp,
		"adventure": Globals.adventure
	};
	
#	if(!Directory.file_exists("user://PlayerStats.json")):
#		var dir = Directory.new();
#		dir.copy("res://Stats/PlayerStats.json", "user://PlayerStats.json");
	
	var file = File.new();
	file.open("user://PlayerStats.json", file.WRITE);
	file.store_line(to_json(data));
	file.close();
	print("Saved!");

func irand_range(x, y):
	return (randi() % (y - x + 1) + x);

func _update_text(node_path, value):
	get_tree().get_root().get_node(node_path).text = String(value);

func _update_bar(path, _max, _value):
	get_tree().get_root().get_node(path).max_value = _max;
	get_tree().get_root().get_node(path).value = _value;

func _center(node_path, txt):
	get_tree().get_root().get_node(node_path).set_bbcode("[center]"+txt+"[/center]");	

func _floating_text(_pos, _color, _value):
	var floating_text = floating_text_node.instance();
	get_tree().get_root().get_node("Main").add_child(floating_text);
	floating_text.color = _color;
	floating_text.rect_position.y = _pos.y - 20;
	floating_text.rect_position.x = _pos.x + rand_range(-5, 5);
	floating_text.text = _value;
	
func _add_description(scene, txt):
	# Add "\n" in the txt
	if(get_tree().get_root().has_node("Sell")):
		get_tree().get_root().get_node("Sell" + "/Inspect/Container/Description").set_bbcode(String(txt));
		return;
	get_tree().get_root().get_node(scene + "/Inspect/Container/Description").set_bbcode(String(txt));