extends Control

var floor_name;

func _ready():
	$TextureButton/RichTextLabel.set_bbcode("[center]" + "Floor " + String(floor_name+1) + "[/center]");
	$TextureButton/RichTextLabel2.set_bbcode("[center]" + "Floor " + String(floor_name+1) + "[/center]");
	$TextureButton.connect("pressed", self, "_input_event");

func _input_event():
	Globals._floor = "floor" + String(floor_name + 1);
	get_tree().change_scene("res://Scenes/Main.tscn");