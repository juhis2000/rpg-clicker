extends Node

var selected_item;
var item_name;

func _ready():
	$CrystalText.text = String(Globals.crystals);

func _process(delta):
	if(Input.is_action_just_pressed("ui_accept")): Funcs._save();

func _on_BuyButton_pressed():
	if(selected_item != null):
		if(Globals.crystals >= Globals.item_json_result[item_name]["buy_for"]):
			Globals.crystals -= Globals.item_json_result[item_name]["buy_for"];
			$CrystalText.text = String(Globals.crystals);
			$ScrollContainer/ItemList.get_child(selected_item).queue_free();
			Globals.items.append(item_name);
			selected_item = null;
		else: print("Not enough crystals!");

func _on_SellButton_pressed():
	pass # replace with function body

func _on_ReturnButton_pressed():
	Funcs._save();
	get_tree().change_scene("res://Scenes/MainMenu.tscn");

func _on_GoToBuyButton_pressed():
	Funcs._save();
	get_tree().change_scene("res://Scenes/Shop.tscn");

func _on_GoToSellButton_pressed():
	Funcs._save();
	get_tree().change_scene("res://Scenes/Sell.tscn");